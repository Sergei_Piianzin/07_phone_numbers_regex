package com.epam.phone_number.app;

import com.epam.phone_number.formatter.FileFormatter;

public class App {
  public static void main(String[] args) {
    String inFile = "src/main/resources/breaking_benjamin.txt";
    String outFile = "src/main/resources/breaking_benjamin_output.txt";
    FileFormatter formatter = new FileFormatter(inFile, outFile);
    Thread formatThread = new Thread(formatter);
    formatThread.start();
    try {
      formatThread.join();
    } catch (InterruptedException e) {
    }
    System.out.println("Done.");
  }
}
