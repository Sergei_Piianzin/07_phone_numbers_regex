package com.epam.phone_number.formatter;

import com.epam.phone_number.reader.FileReader;
import com.epam.phone_number.writer.FileWriter;
import java.util.concurrent.ConcurrentLinkedQueue;

public class FileFormatter implements Runnable {
  private PhoneNumbersFormatter formatter;
  private FileReader reader;
  private FileWriter writer;
  private ConcurrentLinkedQueue<String> modifiedQueue;
  private ConcurrentLinkedQueue<String> inputQueue;

  public FileFormatter(String inFile, String outFile) {
    formatter = new PhoneNumbersFormatter();
    inputQueue = new ConcurrentLinkedQueue<>();
    modifiedQueue = new ConcurrentLinkedQueue<>();
    reader = new FileReader(inFile, inputQueue);
    writer = new FileWriter(outFile, modifiedQueue);
  }

  public void run() {
    Thread input = new Thread(reader);
    Thread output = new Thread(writer);
    input.start();
    output.start();

    boolean formatterWorking = true;
    while (formatterWorking) {
      formatLines();
      if (!input.isAlive()) {
        formatLines();
        formatterWorking = false;
      }
    }
    try {
      input.join();
      writer.stopWriting();
      output.join();
    } catch (InterruptedException e) {
    }
  }

  private void formatLines() {
    while (!inputQueue.isEmpty()) {
      String inputLine = inputQueue.poll();
      String modifiedLine = formatter.format(inputLine);
      modifiedQueue.add(modifiedLine);
    }
  }
}
