package com.epam.phone_number.formatter;

@FunctionalInterface
public interface Formatable {
  String format(String input);
}
