package com.epam.phone_number.formatter;

import java.util.List;

import com.epam.phone_number.number.PhoneNumbersFinder;
import com.epam.phone_number.number.PhoneNumbersReplacer;
import com.epam.phone_number.utils.Range;

public class PhoneNumbersFormatter implements Formatable {
  private PhoneNumbersFinder numbersFinder;
  private PhoneNumbersReplacer numbersReplacer;

  public PhoneNumbersFormatter() {
    numbersFinder = new PhoneNumbersFinder();
    numbersReplacer = new PhoneNumbersReplacer();
  }

  /**
   * Returns a string that contain formatted numbers. +Y(XXX) XXX XX XX to YXXXXXXXXXX It takes
   * ranges where phone numbers are being and formats these numbers.
   */
  public String format(String input) {
    List<Range> ranges = numbersFinder.getRanges(input);
    return numbersReplacer.getConvertedString(input, ranges);
  }
}
