package com.epam.phone_number.number;

public class PhoneNumberConverter {
  public String changeNumber(String number) {
    StringBuilder sb = new StringBuilder(number.length());
    for (int i = 0; i < number.length(); ++i) {
      Character c = number.charAt(i);
      if (Character.isDigit(c)) {
        sb.append(c);
      }
    }
    return sb.toString();
  }
}
