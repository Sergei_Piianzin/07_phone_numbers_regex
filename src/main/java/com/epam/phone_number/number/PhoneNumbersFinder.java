package com.epam.phone_number.number;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.epam.phone_number.utils.Range;

public class PhoneNumbersFinder {
  private Pattern pattern;

  public PhoneNumbersFinder() {
    pattern = Pattern.compile("\\+\\d\\(\\d{3}\\)\\s\\d{3}\\s\\d{2}\\s\\d{2}");
  }

  public List<Range> getRanges(String input) {
    Matcher matcher = pattern.matcher(input);
    List<Range> matches = new ArrayList<>();
    while (matcher.find()) {
      matches.add(new Range(matcher.start(), matcher.end()));
    }
    return matches;
  }
}
