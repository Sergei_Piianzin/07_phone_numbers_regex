package com.epam.phone_number.number;

import java.util.List;

import com.epam.phone_number.utils.Range;

public class PhoneNumbersReplacer {
  PhoneNumberConverter converter;

  public PhoneNumbersReplacer() {
    converter = new PhoneNumberConverter();
  }

  public String getConvertedString(String input, List<Range> ranges) {
    if (input == null) {
      return null;
    }
    if (ranges == null) {
      return input;
    }

    StringBuilder sb = new StringBuilder(input);
    if (ranges.size() > 0) {
      for (int i = ranges.size() - 1; i >= 0; --i) {
        Range r = ranges.get(i);
        int begin = r.getBegin();
        int end = r.getEnd();
        String number = sb.substring(begin, end);
        sb = sb.replace(begin, end, converter.changeNumber(number));
      }
    }

    return sb.toString();
  }
}
