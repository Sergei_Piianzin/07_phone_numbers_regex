package com.epam.phone_number.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileReader implements Runnable {
  private String filePath;
  private ConcurrentLinkedQueue<String> queue;
  private static final Logger logger  = LoggerFactory.getLogger("READ");

  public FileReader(String filePath, ConcurrentLinkedQueue<String> queue) {
    this.filePath = filePath;
    this.queue = queue;
  }
  
  public void run() {
    try (BufferedReader reader =
        Files.newBufferedReader(Paths.get(filePath), Charset.forName("cp1251"))) {
      String line = null;
      while ((line = reader.readLine()) != null) {
        queue.add(line);
        logger.info("add");
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}

