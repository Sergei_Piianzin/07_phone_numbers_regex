package com.epam.phone_number.utils;

public class Range {
  private int begin;
  private int end;

  public Range(int begin, int end) {
    this.begin = begin;
    this.end = end;
  }

  public int getBegin() {
    return begin;
  }

  public int getEnd() {
    return end;
  }
}
