package com.epam.phone_number.writer;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.WRITE;

public class FileWriter implements Runnable {
  private boolean isWriting;
  private String pathToFile;
  private ConcurrentLinkedQueue<String> queue;
  private static final Logger logger = LoggerFactory.getLogger("WRITE");

  public FileWriter(String pathToFile, ConcurrentLinkedQueue<String> queue) {
    this.pathToFile = pathToFile;
    this.queue = queue;
  }

  public void run() {
    try (BufferedWriter writer =
        Files.newBufferedWriter(Paths.get(pathToFile), Charset.forName("cp1251"), CREATE, WRITE)) {
      isWriting = true;
      while (isWriting) {
        writeLines(writer);
      }
      writeLines(writer);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void writeLines(BufferedWriter writer) throws IOException {
    while (!queue.isEmpty()) {
      String line = queue.poll();
      logger.info("poll");
      writer.write(line);
      writer.newLine();
    }
  }

  public void stopWriting() {
    isWriting = false;
  }
}
