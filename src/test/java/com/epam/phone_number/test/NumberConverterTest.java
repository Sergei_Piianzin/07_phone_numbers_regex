package com.epam.phone_number.test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.epam.phone_number.number.PhoneNumberConverter;
import com.epam.phone_number.number.PhoneNumbersReplacer;

public class NumberConverterTest {
  
  @Test
  public void shouldChangeNumberFormat() {
    PhoneNumberConverter numberConverter = new PhoneNumberConverter();
    String number = numberConverter.changeNumber("+1(431) 542 56 12");
    assertThat(number, is("14315425612"));
  }
}
