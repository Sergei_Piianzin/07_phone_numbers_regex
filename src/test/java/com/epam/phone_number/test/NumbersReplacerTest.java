package com.epam.phone_number.test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.phone_number.number.PhoneNumbersReplacer;
import com.epam.phone_number.utils.Range;

public class NumbersReplacerTest {

  PhoneNumbersReplacer numbersReplacer;
  String input;
  String right;

  @Before
  public void init() {
    numbersReplacer = new PhoneNumbersReplacer();
    input =
        "Notification has been sent to +4(351) 455 22 44 successfully. +1(431) 542 56 12 is unreachable";
    right = "Notification has been sent to 43514552244 successfully. 14315425612 is unreachable";
  }

  @Test
  public void shouldReturnLineWithConvertedNumbers() {
    List<Range> ranges = new ArrayList(Arrays.asList(new Range(30, 47), new Range(62, 79)));
    String convertedString = numbersReplacer.getConvertedString(input, ranges);
    assertThat(convertedString, is(right));
  }

  @Test
  public void shouldReturnOldLineWithEmptyRanges() {
    List<Range> ranges = new ArrayList<>();
    String convertedString = numbersReplacer.getConvertedString(input, ranges);
    assertThat(convertedString, is(input));
  }
}
